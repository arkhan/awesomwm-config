-- {{{ Basic
theme = {}
theme.confdir                       = os.getenv("HOME") .. "/.config/awesome/themes/Darkness"
theme.wallpaper                     = theme.confdir .. "/wallpaper/3.jpg"
-- }}}

-- {{{ Fonts
theme.font                          = "Termsyn 9"
theme.dfont                         = "Termsyn-9"
-- }}}

-- {{{ Colors

theme.bg_normal                     = "#0000002A"
theme.bg_focus                      = "#0000002A"
theme.bg_urgent                     = "#ff00002A"
theme.bg_systray                    = theme.bg_normal

theme.bg_dmenu_normal               = "#000000"
theme.bg_dmenu_focus                = "#000000"

theme.fg_normal                     = "#777777"
theme.fg_focus                      = "#DDDDDD"
theme.fg_urgent                     = "#CC9393"

theme.border_width                  = 2
theme.border_normal                 = "#070707"
theme.border_focus                  = "#292929"
theme.titlebar_bg_focus             = "#0000002A"

theme.taglist_fg_focus              = "#dddcff"
theme.taglist_bg_focus              = "#0000002A"

theme.tooltip_bg_color              = "#0707072A"
theme.tooltip_fg_color              = "#dddcff"
theme.tooltip_border_width          = 2
theme.tooltip_border_color          = "#121212"
--}}}

-- {{{ Awful
theme.awful_widget_height           = "10"
theme.awful_widget_margin_top       = "2"
-- }}}

-- {{{ Menu
theme.menu_height                   = "20"
theme.menu_width                    = "125"
-- }}}

-- {{{ Tag list images
theme.taglist_squares_sel           = theme.confdir .. "/taglist/square_sel.png"
theme.taglist_squares_unsel         = theme.confdir .. "/taglist/square_unsel.png"
-- }}}

-- {{{ Tasklis float icon
theme.tasklist_floating_icon        = theme.confdir .. "/floating.png"
-- }}}

-- {{{ Widgets
theme.ac                            = theme.confdir .. "/icons/ac.png"
theme.bat                           = theme.confdir .. "/icons/bat.png"
theme.bat_low                       = theme.confdir .. "/icons/bat_low.png"
theme.bat_no                        = theme.confdir .. "/icons/bat_no.png"
theme.disk                          = theme.confdir .. "/icons/disk.png"
theme.vol                           = theme.confdir .. "/icons/vol.png"
theme.vol_low                       = theme.confdir .. "/icons/vol_low.png"
theme.vol_no                        = theme.confdir .. "/icons/vol_no.png"
theme.vol_mute                      = theme.confdir .. "/icons/vol_mute.png"
theme.widget_bg                     = theme.confdir .. "/icons/widget_bg.png"
-- }}}

--{{{ Layout
theme.layout_txt_tile               = "[t]"
theme.layout_txt_tileleft           = "[l]"
theme.layout_txt_tilebottom         = "[b]"
theme.layout_txt_tiletop            = "[tt]"
theme.layout_txt_fairv              = "[fv]"
theme.layout_txt_fairh              = "[fh]"
theme.layout_txt_spiral             = "[s]"
theme.layout_txt_dwindle            = "[d]"
theme.layout_txt_max                = "[m]"
theme.layout_txt_fullscreen         = "[F]"
theme.layout_txt_magnifier          = "[M]"
theme.layout_txt_floating           = "[*]"

-- lain related
theme.useless_gap_width             = 10
theme.layout_txt_cascade            = "[cascade]"
theme.layout_txt_cascadetile        = "[cascadetile]"
theme.layout_txt_centerwork         = "[centerwork]"
theme.layout_txt_termfair           = "[termfair]"
theme.layout_txt_centerfair         = "[centerfair]"
theme.layout_txt_uselessfair        = "[uf]"
theme.layout_txt_uselessfairh       = "[ufh]"
theme.layout_txt_uselesspiral       = "[us]"
theme.layout_txt_uselessdwindle     = "[ud]"
theme.layout_txt_uselesstile        = "[ut]"
theme.layout_txt_uselesstileleft    = "[utl]"
theme.layout_txt_uselesstiletop     = "[utt]"
theme.layout_txt_uselesstilebottom  = "[utb]"

-- }}}

-- }}}

-- {{{ Menu Icons
theme.awesome_icon                  = theme.confdir .. "/icons/awesome.png"
theme.menu_submenu_icon             = theme.confdir .. "/icons/submenu.png"
return theme
-- }}}
