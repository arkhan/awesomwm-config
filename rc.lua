-- Standard awesome library
awful               = require("awful")
awful.autofocus     = require("awful.autofocus")
awful.rules         = require("awful.rules")
gears               = require("gears")
eminent         	= require("eminent")
tyrannical          = require("tyrannical")
-- Widget and layout library
awesompd            = require("awesompd/awesompd")
drop                = require("attachdrop")
lain                = require("lain")
wibox               = require("wibox")
-- Theme handling library
beautiful           = require("beautiful")
freedesktop         = require("freedesktop")
-- Notification library
naughty             = require("naughty")
menubar             = require("menubar")

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = err })
        in_error = false
    end)
end
-- }}}

-- {{{ Variable Definitions

os.setlocale(os.getenv("LANG"))

-- Useful Paths
home = os.getenv("HOME")
confdir = home .. "/.config/awesome"
scriptdir = home .. "/.bin"
scr_res = "1920x1200"

-- Choose Your Theme
beautiful.init(confdir .. "/themes/Darkness/theme.lua")

-- {{{ Wallpaper
if beautiful.wallpaper then
    for s = 1, screen.count() do
        gears.wallpaper.maximized(beautiful.wallpaper, s, true)
    end
end
-- }}}

--{{{ Naughty

naughty.config.defaults.timeout = 5
naughty.config.defaults.margin = "5"
naughty.config.defaults.ontop = true
naughty.config.defaults.screen = 1
naughty.config.defaults.position = "top_right"
naughty.config.defaults.border_width = 1
naughty.config.defaults.border_color = beautiful.tooltip_border_color
naughty.config.defaults.bg = "#000000"
naughty.config.defaults.fg = "#FFFFFF"

-- This is used later as the default terminal and editor to run.
terminal        = "urxvtc -geometry 140x40 -name Terminal -icon " .. freedesktop.utils.lookup_icon({ icon='terminal' })
drop_terminal   = "urxvtc -pe tabbedex -name Drop-Terminal -icon " .. freedesktop.utils.lookup_icon({ icon='guake' })
editor          = "vim"
editor_cmd      = terminal .. " -e " .. editor
gui_editor      = "gvim"
browser         = "firefox"
fileman         = "dolphin " .. "/home/data/"
cli_fileman     = terminal .. " -title Ranger -icon " .. freedesktop.utils.lookup_icon({ icon='file-manager' }) .. " -e ranger " .. "/home/data"
music           = terminal .. " -geometry 140x50 -title Music -icon " .. freedesktop.utils.lookup_icon({ icon='cantata' }) .. " -e ncmpcpp "
wifi            = terminal .. " -title Network -- -icon " .. freedesktop.utils.lookup_icon({ icon='network-wired'  }) .. " -e nmtui "
chat            = terminal .. " -title Chat -icon "  .. freedesktop.utils.lookup_icon({ icon='pidgin'  })  ..  " -e weechat "
tmux            = terminal .. " -title Tmux -icon " .. freedesktop.utils.lookup_icon({ icon='terminator' }) .. " -e tmux new -s arkhan "

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"
altkey = "Mod1"

-- Table of layouts to cover with awful.layout.inc, order matters.
local layouts = {
    awful.layout.suit.floating,
    awful.layout.suit.fair,
    awful.layout.suit.tile,
    lain.layout.uselessfair.horizontal,
    lain.layout.uselesstile,
    lain.layout.uselessfair,
    lain.layout.termfair,
    lain.layout.centerfair,
    lain.layout.uselesspiral.dwindle
}
-- }}}

-- First, set some settings
tyrannical.settings.block_children_focus_stealing = true
tyrannical.settings.group_children = true

tyrannical.settings.default_layout = lain.layout.uselessfair
tyrannical.settings.mwfact = 0.66

-- Setup some tags
tyrannical.tags = {

    {
        name = "Internet",
        init = true,
        exclusive = true,
        screen = screen.count()>1 and 2 or 1,
        layout = lain.layout.uselessfair,
        class = {
            "Opera", "Firefox", "Rekonq", "Dillo", "Arora", "Iron",
            "Chromium", "Google-chrome-stable", "nightly", "minefield",
        }
    } ,
    {
        name = "Mail",
        init = false,
        position = 10,
        exclusive = true,
        layout = lain.layout.uselessfair,
        class = {
			"Kmail", "Kmail:New", "Kmail:Re", "Akonadi_imap_resource",
			"evolution", "Thunderbird",
		}
    } ,
    {
        name = "Files",
        init = false,
        exclusive = true,
        screen = 1,
        layout = lain.layout.uselessfair,
        class  = {
            "Thunar", "Konqueror", "Dolphin", "ark", "Nautilus","emelfm",
            "URxvt:Ranger", "Dfm", "Dolphin:Move", "Dolphin:Copy",
        }
    } ,
    {
        name = "Chat",
        init = false,
        exclusive = true,
        layout = lain.layout.uselessfair,
        class = {
            "URxvt:Chat"
        }
    } ,
    {
        name = "Develop",
        init = false,
        exclusive = true,
        screen = 1,
        clone_on = 2,
        layout = lain.layout.uselessfair,
        class = {
            "Kate", "KDevelop", "Codeblocks", "Code::Blocks" , "DDD", "kate4", "Geany",
            "jetbrains-pychar", "jetbrains-pycharm"
        }
    } ,
    {
        name = "Doc",
        init = false,
        exclusive = true,
        layout = lain.layout.uselessfair,
        class = {
            "Assistant", "Okular", "Evince", "EPDFviewer", "xpdf",
            "Xpdf", "Zathura"
        }
    } ,
    {
        name = "Office",
        init = false,
        position = 10,
        exclusive = true,
        layout = lain.layout.uselessfair,
        class  = {
			"N/A", "LibreOffice 4.4","libreoffice-startcenter", "libreoffice-writer", "libreoffice-calc", "libreoffice-impress",
			"libreoffice-base", "libreoffice-math", "libreoffice-draw", "com-almworks-launcher-Launcher",
            "OOWriter"      , "OOCalc"         , "OOMath"    , "OOImpress"    , "OOBase"       ,
            "SQLitebrowser" , "Silverun"       , "Workbench" , "KWord"        , "KSpread"      ,
            "KPres","Basket", "openoffice.org" , "OpenOffice.*"               ,
		}
    } ,
    {
        name = "Multimedia",
        init = false,
        exclusive = true,
        layout = lain.layout.uselessfair,
        class = {
            "URxvt:MPD", "Cantata",
        }
    } ,
    {
        name = "Config",
        init = false ,
        position = 10,
        exclusive = false,
        layout = lain.layout.uselessfair,
        class = {
			"Systemsettings", "Kcontrol", "gconf-editor", "Lxappearance",
			"Kwalletd"
		}
    } ,
    {
        name = "Download",
        init = false ,
        position = 10,
        exclusive = false,
        layout = lain.layout.uselessfair,
        class = {
			"JDownloader"
		}
    } ,
    {
        name = "FullScreen",
        init = false ,
        position = 10,
        exclusive = false,
        layout = awful.layout.suit.max.fullscreen,
        class = {
           "Plugin-container"
	}
    } ,
    {
       name = "Movie",
       init = false,
       position = 10,
       exclusive = false,
       layout = lain.layout.uselessfair,
       class = {
          "MPV:Movie"
       }
    }
}

-- Ignore the tag "exclusive" property for the following clients (matched by classes)
tyrannical.properties.intrusive = {
    "xterm", "urxvt", "aterm", "URxvt", "XTerm", "konsole", "terminator", "gnome-terminal",
    "URxvt:Terminal", "URxvt:Drop", "URxvt:Tmux", "XTerm:Terminal",
    "ksnapshot"     , "pinentry"       , "gtksu"     , "kcalc"        , "xcalc"               ,
    "feh"           , "Gradient editor", "About KDE" , "Paste Special", "Background color"    ,
    "kcolorchooser" , "plasmoidviewer" , "Xephyr"    , "kruler"       , "plasmaengineexplorer",
}

-- Ignore the tiled layout for the matching clients
tyrannical.properties.floating = {
    "xterm", "urxvt", "aterm", "URxvt", "XTerm", "konsole", "terminator", "gnome-terminal",
    "URxvt:Terminal", "URxvt:Drop", "URxvt:Tmux", "URxvt:MPD", "XTerm:Terminal", "Kmail:New", "Kmail:Re",
    "Dolphin:Move", "Dolphin:Copy",
    "MPlayer"      , "pinentry"        , "ksnapshot"  , "pinentry"     , "gtksu"          ,
    "xine"         , "feh"             , "kmix"       , "kcalc"        , "xcalc"          ,
    "yakuake"      , "Select Color$"   , "kruler"     , "kcolorchooser", "Paste Special"  ,
    "New Form"     , "Insert Picture"  , "kcharselect", "mythfrontend" , "plasmoidviewer"
}

-- Make the matching clients (by classes) on top of the default layout
tyrannical.properties.ontop = {
    "Xephyr"       , "ksnapshot"       , "kruler"
}

-- Force the matching clients (by classes) to be centered on the screen on init
tyrannical.properties.centered = {
   "kcalc", "MPV:Movie", "URxvt:Terminal", "URxvt:Chat", "XTerm:Terminal", "URxvt:MPD",
   "URxvt:Tmux", "Dolphin:Move", "Dolphin:Copy", "Kmail:New", "Kmail:Re",
}
-- Do not honor size hints request for those classes
tyrannical.properties.size_hints_honor = { xterm = false, URxvt = false, aterm = false, sauer_client = false, mythfrontend  = false}

-- }}}

-- {{{ Menu
-- Create a laucher widget and a main menu

require("utils.menu")

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}

-- {{{ Widgets

-- markup
markup	= lain.util.markup
gray 	= beautiful.fg_normal
white	= beautiful.fg_focus
red		= "#EB8F8F"
green	= "#8FEB8F"

-- Clock & Date
mytextclock = awful.widget.textclock(markup(gray, "| ") .. markup(white, "%A %d %B ") .. markup(gray, "/") .. markup(white, " %H:%M "))

-- Calendar with the Clock
lain.widgets.calendar:attach(mytextclock, {
    fg = "#FFFFFF",
    bg = "#000000",
})

-- /home fs
diskicon = wibox.widget.imagebox(beautiful.disk)
diskbar = awful.widget.progressbar()
diskbar:set_color(beautiful.fg_normal)
diskbar:set_width(55)
diskbar:set_ticks(true)
diskbar:set_ticks_size(6)
diskbar:set_background_color("#000000")
diskmargin = wibox.layout.margin(diskbar, 2, 7)
diskmargin:set_top(6)
diskmargin:set_bottom(6)
fshomeupd = lain.widgets.fs({
    partition = "/home",
    settings  = function()
        if fs_now.used < 90 then
            diskbar:set_color(beautiful.fg_normal)
        else
            diskbar:set_color("#EB8F8F")
        end
        diskbar:set_value(fs_now.used / 100)
    end
})
diskwidget = wibox.widget.background(diskmargin)
diskwidget:set_bgimage(beautiful.widget_bg)

local fs = {}

local notification  = nil
fs_notification_preset = { fg = beautiful.fg_normal }

function fs:hide()
    if notification ~= nil then
        naughty.destroy(notification)
        notification = nil
    end
end

function fs:show(t_out)
    fs:hide()

    local f = io.popen(confdir .. "/lain/scripts/dfs")
    ws = f:read("*a"):gsub("\n*$", "")
    f:close()

    notification = naughty.notify({
        preset = fs_notification_preset,
        text = ws,
        timeout = t_out,
    })
end

diskwidget:connect_signal('mouse::enter', function () fs:show(0) end)
diskwidget:connect_signal('mouse::leave', function () fs:hide() end)

-- Volume widget
-- ALSA volume bar
volicon = wibox.widget.imagebox(beautiful.vol)
volume = lain.widgets.alsabar({width = 55, ticks = true, ticks_size = 6,
card = "0", step = "2%",
settings = function()
    if volume_now.status == "off" then
        volicon:set_image(beautiful.vol_mute)
    elseif volume_now.level == 0 then
        volicon:set_image(beautiful.vol_no)
    elseif volume_now.level <= 50 then
        volicon:set_image(beautiful.vol_low)
    else
        volicon:set_image(beautiful.vol)
    end
end,
colors =
{
    background = "#000000",
    mute = red,
    unmute = beautiful.fg_normal
}})
volmargin = wibox.layout.margin(volume.bar, 2, 7)
volmargin:set_top(6)
volmargin:set_bottom(6)
volumewidget = wibox.widget.background(volmargin)
volumewidget:set_bgimage(beautiful.widget_bg)

-- Music

musicwidget = awesompd:create()
musicwidget.font = beautiful.font
musicwidget.scrolling = true
musicwidget.output_size = 200
musicwidget.update_interval = 10
musicwidget.path_to_icons = confdir .. "/awesompd/icons"
musicwidget.jamendo_format = awesompd.FORMAT_MP3
musicwidget.show_album_cover = true
musicwidget.album_cover_size = 100
musicwidget.mpd_config = home .. "/.config/mpd/mpd.conf"
musicwidget.browser = "firefox"
musicwidget.ldecorator = " "
musicwidget.rdecorator = " "
musicwidget.servers = {
    { server = "localhost", port = 6600 },
}
musicwidget.background = "#000000"
musicwidget:register_buttons({  { "", awesompd.MOUSE_LEFT, musicwidget:command_toggle() },
                                { "Control", awesompd.MOUSE_SCROLL_UP, musicwidget:command_prev_track() },
                                { "Control", awesompd.MOUSE_SCROLL_DOWN, musicwidget:command_next_track() },
                                { "", awesompd.MOUSE_SCROLL_UP, musicwidget:command_volume_up() },
                                { "", awesompd.MOUSE_SCROLL_DOWN, musicwidget:command_volume_down() },
                                { "", awesompd.MOUSE_RIGHT, musicwidget:command_show_menu() },
                                { "", "XF86AudioLowerVolume", musicwidget:command_volume_down() },
                                { "", "XF86AudioRaiseVolume", musicwidget:command_volume_up() },
                                { modkey, "Pause", musicwidget:command_playpause() } })
musicwidget:run()

-- Net Info
wifiwidget = lain.widgets.base({
  cmd = "iwgetid -r",
  settings = function()
    if output ~= nil and output ~= '' then
      widget:set_markup(output)
    else
      widget:set_markup('')
    end
  end
})

local wifinotification
wifiwidget:connect_signal("mouse::enter", function()
    local f = assert(io.popen("iwgetid -ar"))
    local wifiaccesspoint = f:read()
    f:close()

    local f = io.popen("ip link show | cut -d' ' -f2,9")
    local ws = f:read("*a")
    f:close()

    local ws = ws:match("%w+: UP")

    f = assert(io.popen("iwconfig ".. ws:gsub(": UP", "") .." | grep 'Link Quality' | awk '{print $2}' | awk -F'=' '{print $2}'"))
    local wifiquality = f:read()
    f:close()

    f = io.popen("ip addr | grep 'inet ' | grep -v '127.0.0.1' | cut -d ':' -f2 | awk '{print $2}'")
    local ipaddress = f:read()
    f:close()

wifinotification = naughty.notify({
    text = "IP: " .. ipaddress .. "\nAccess Point: " .. wifiaccesspoint .. "\nLink Quality: " .. wifiquality,
    position = "top_right",
    timeout = 0
  })
end)

wifiwidget:connect_signal("mouse::leave", function()
    if (wifinotification ~= nil) then
        naughty.destroy(wifinotification)
        wifinotification = nil
    end
end)

netwidget = lain.widgets.net({
    settings = function()
        widget:set_markup(markup(gray, " ") .. markup(white, net_now.received) .. markup(gray, " / ") .. markup(white, net_now.sent) .. markup(gray, " |"))
    end
})

netwidget:buttons(awful.util.table.join(
    awful.button({ }, 1, function () awful.util.spawn(wifi) end)
))

-- Battery
baticon = wibox.widget.imagebox(beautiful.bat)
batbar = awful.widget.progressbar()
batbar:set_color(beautiful.fg_normal)
batbar:set_width(55)
batbar:set_ticks(true)
batbar:set_ticks_size(6)
batbar:set_background_color("#000000")
batmargin = wibox.layout.margin(batbar, 2, 7)
batmargin:set_top(6)
batmargin:set_bottom(6)
batupd = lain.widgets.bat({
    settings = function()
       if bat_now.perc == "N/A" or bat_now.status == "Not present" then
            bat_perc = 100
            baticon:set_image(beautiful.ac)
        elseif bat_now.status == "Charging" then
            bat_perc = tonumber(bat_now.perc)
            baticon:set_image(beautiful.ac)

            if bat_perc >= 95 then
                batbar:set_color(green)
            elseif bat_perc > 50 then
                batbar:set_color(beautiful.fg_normal)
            elseif bat_perc > 15 then
                batbar:set_color(beautiful.fg_normal)
            else
                batbar:set_color(red)
            end
        elseif bat_now.status == "Unknown" then
            bat_perc = tonumber(bat_now.perc)
            baticon:set_image(beautiful.ac)

            if bat_perc >= 95 then
                batbar:set_color(green)
            elseif bat_perc > 50 then
                batbar:set_color(beautiful.fg_normal)
            elseif bat_perc > 15 then
                batbar:set_color(beautiful.fg_normal)
            else
                batbar:set_color(red)
            end
        else
            bat_perc = tonumber(bat_now.perc)

            if bat_perc >= 95 then
                batbar:set_color(green)
            elseif bat_perc > 50 then
                batbar:set_color(beautiful.fg_normal)
                baticon:set_image(beautiful.bat)
            elseif bat_perc > 15 then
                batbar:set_color(beautiful.fg_normal)
                baticon:set_image(beautiful.bat_low)
            else
                batbar:set_color(red)
                baticon:set_image(beautiful.bat_no)
            end
        end
        batbar:set_value(bat_perc / 100)
    end
})
batwidget = wibox.widget.background(batmargin)
batwidget:set_bgimage(beautiful.widget_bg)

-- }}}

-- {{{ Spacers

-- Separators

rbracket = wibox.widget.textbox()
rbracket:set_text(']')
lbracket = wibox.widget.textbox()
lbracket:set_text('[')
line = wibox.widget.textbox()
line:set_text('│')
space = wibox.widget.textbox()
space:set_text(' ')
mytextbox = wibox.widget.textbox()
-- }}}

-- Create a wibox for each screen and add it
mywibox = {}
mybottomwibox = {}
txtlayoutbox = {}
mytaglist = {}
mytaglist.buttons = awful.util.table.join(
                    awful.button({ }, 1, awful.tag.viewonly),
                    awful.button({ modkey }, 1, awful.client.movetotag),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, awful.client.toggletag),
                    awful.button({ }, 4, function(t) awful.tag.viewnext(awful.tag.getscreen(t)) end),
                    awful.button({ }, 5, function(t) awful.tag.viewprev(awful.tag.getscreen(t)) end)
                    )
mytasklist = {}
mytasklist.buttons = awful.util.table.join(
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  -- Without this, the following
                                                  -- :isvisible() makes no sense
                                                  c.minimized = false
                                                  if not c:isvisible() then
                                                       awful.tag.viewonly(c:tags()[1])
                                                  end
                                                  -- This will also un-minimize
                                                  -- the client, if needed
                                                  client.focus = c
                                                  c:raise()
                                              end
                                          end),
                     awful.button({ }, 3, function ()
                                              if instance then
                                                  instance:hide()
                                                  instance = nil
                                              else
                                                  instance = awful.menu.clients({ width=250 })
                                              end
                                          end),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                              if client.focus then client.focus:raise() end
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                              if client.focus then client.focus:raise() end
                                          end))

function updatelayoutbox(layout, s)
    local screen = s or 1
    local txt_l = beautiful["layout_txt_" .. awful.layout.getname(awful.layout.get(screen))] or ""
    layout:set_text(txt_l)
end

for s = 1, screen.count() do

    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
	txtlayoutbox[s] = wibox.widget.textbox(beautiful["layout_txt_" .. awful.layout.getname(awful.layout.get(s))])
    awful.tag.attached_connect_signal(s, "property::selected", function ()
        updatelayoutbox(txtlayoutbox[s], s)
    end)
    awful.tag.attached_connect_signal(s, "property::layout", function ()
        updatelayoutbox(txtlayoutbox[s], s)
    end)
    txtlayoutbox[s]:buttons(awful.util.table.join(
            awful.button({}, 1, function() awful.layout.inc(layouts, 1) end),
            awful.button({}, 3, function() awful.layout.inc(layouts, -1) end),
            awful.button({}, 4, function() awful.layout.inc(layouts, 1) end),
            awful.button({}, 5, function() awful.layout.inc(layouts, -1) end)))

    -- Create a taglist widget
    mytaglist[s] = awful.widget.taglist(s, awful.widget.taglist.filter.all, mytaglist.buttons)

    -- Create a tasklist widget
    mytasklist[s] = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, mytasklist.buttons)

    -- Create the wibox
    mywibox[s] = awful.wibox({ position = "top", screen = s, height = 18 })
    
    left_layout = wibox.layout.fixed.horizontal()
    left_layout:add(txtlayoutbox[s])
    left_layout:add(space)
    left_layout:add(mytaglist[s])
    left_layout:add(space)

    right_layout = wibox.layout.fixed.horizontal()
    right_layout:add(space)
    right_layout:add(space)
    right_layout:add(wifiwidget)
    right_layout:add(netwidget)
    right_layout:add(space)
    right_layout:add(diskicon)
    right_layout:add(diskwidget)
    right_layout:add(space)
    right_layout:add(baticon)
    right_layout:add(batwidget)
    right_layout:add(space)
    right_layout:add(volicon)
    right_layout:add(volumewidget)
    right_layout:add(space)
    right_layout:add(mytextclock)

    local layout = wibox.layout.align.horizontal()
    layout:set_left(left_layout)
    layout:set_middle(mytextbox)
    layout:set_right(right_layout)

    mywibox[s]:set_widget(layout)

    mybottomwibox[s] = awful.wibox({ position = "bottom", screen = s, height = 16 })
    mybottomwibox[s].visible = false

    bottom_right_layout = wibox.layout.fixed.horizontal()
    if s == 1 then bottom_right_layout:add(wibox.widget.systray()) end
    bottom_right_layout:add(space)
    bottom_right_layout:add(musicwidget.widget)

    bottom_layout = wibox.layout.align.horizontal()
    bottom_layout:set_middle(mytasklist[s])
    bottom_layout:set_right(bottom_right_layout)
    mybottomwibox[s]:set_widget(bottom_layout)

end
-- }}}

-- {{{ Mouse bindings
root.buttons(awful.util.table.join(
    awful.button({ }, 3, function () mymainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)
))
-- }}}

-- {{{ Key bindings
globalkeys = awful.util.table.join(
    awful.key({ modkey,           }, "Left",   awful.tag.viewprev       ),
    awful.key({ modkey,           }, "Right",  awful.tag.viewnext       ),
    awful.key({ modkey,           }, "Escape", awful.tag.history.restore),

    awful.key({ modkey,           }, "j",
        function ()
            awful.client.focus.byidx( 1)
            if client.focus then client.focus:raise() end
        end),
    awful.key({ modkey,           }, "k",
        function ()
            awful.client.focus.byidx(-1)
            if client.focus then client.focus:raise() end
        end),
    awful.key({ modkey,           }, "w", function () mymainmenu:show() end),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end),
    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end),
    awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end),
    awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto),
    awful.key({ modkey,           }, "Tab",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end),

    -- ScreenShot
    awful.key({                   }, "Print",      function() os.execute(scriptdir .. "/screenshot") end),

    -- Show/Hide Wibox
    awful.key({ modkey,           }, "F1", function ()
        mywibox[mouse.screen].visible = not mywibox[mouse.screen].visible
    end),

    awful.key({ modkey,           }, "F2", function ()
        mybottomwibox[mouse.screen].visible = not mybottomwibox[mouse.screen].visible
    end),

    -- Standard program
    awful.key({ modkey,           }, "p",     function () awful.util.spawn(music)  end),
    awful.key({ modkey,           }, "t",     function () awful.util.spawn(fileman) end),
    awful.key({ modkey,           }, "f",     function () awful.util.spawn(cli_fileman) end),
    awful.key({ modkey,           }, "b",     function () awful.util.spawn(browser) end),
    awful.key({ modkey,           }, "i",     function () awful.util.spawn(chat) end),
    awful.key({ modkey,           }, "d",     function () awful.util.spawn(torrent) end),
    awful.key({ modkey,           }, "Return",function () awful.util.spawn(terminal) end),
    awful.key({ modkey, "Control" }, "Return",function () awful.util.spawn(tmux) end),
    awful.key({ modkey, "Control" }, "r", awesome.restart),
    awful.key({ modkey, "Shift"   }, "q", awesome.quit),

    -- Record a Screencast
    awful.key({ modkey,       }, "F7",      function() awful.util.spawn(scriptdir .. "/start_screencast") end),
    awful.key({ modkey,       }, "F8",      function() awful.util.spawn(scriptdir .. "/stop_screencast") end),

    -- attach terminal to F12
    awful.key({               }, "F12",
        function ()
           drop.toggle(drop_terminal, "left", "top", 0.9, 0.8)
        end),

    awful.key({ modkey,    }, "F12",
        function ()
           drop.attach(drop_terminal)
        end),

    -- Volume control
    awful.key({ altkey }, "Up",
        function ()
            os.execute("pamixer --increase 1")
            volume.update()
        end),
    awful.key({ altkey }, "Down",
        function ()
            os.execute("pamixer --decrease 1")
            volume.update()
        end),
    awful.key({ altkey }, "m",
        function ()
            os.execute("pamixer --toggle-mute")
            volume.update()
        end),

    awful.key({ modkey,           }, "l",     function () awful.tag.incmwfact( 0.05)    end),
    awful.key({ modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)    end),
    awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1)      end),
    awful.key({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1)      end),
    awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1)         end),
    awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1)         end),
    awful.key({ modkey,           }, "space", function () awful.layout.inc(layouts,  1) end),
    awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(layouts, -1) end),

    awful.key({ modkey, "Control" }, "n", awful.client.restore),

    -- Show Open Window's

    awful.key({ altkey            }, "Tab",
        function ()
            menu = {
				theme = {
					width = 300,
					height = 20,
					border=1,
					border_color = beautiful.border_focus,
                    bg_normal = "#000000"
				}
			}
            menu.items = clsmenu()
            local m = awful.menu(menu)
            m:show()
	end),

	-- Show Open DropDown Terminal

    awful.key({ altkey, "Control" }, "Tab",
        function ()
            menu = {
				theme = {
					width = 300,
					height = 20,
					border=1,
					border_color = beautiful.border_focus,
                    bg_normal = "#000000"
				}
			}
            menu.items = clsmenu_drop()
            local m = awful.menu(menu)
            m:show()
	end),

    -- Prompt
    -- awful.key({ modkey },            "r",     function () mypromptbox[mouse.screen]:run() end),
    awful.key({ modkey            }, "r",
        function()
            os.execute(
                "dmenu_run -b -i -p 'Run:" ..
                "' -nb '" .. beautiful.bg_dmenu_normal ..
                "' -nf '" .. beautiful.fg_normal ..
                "' -sb '" .. beautiful.bg_dmenu_focus  ..
                "' -sf '" .. beautiful.fg_focus  ..
                "' -fn '" .. beautiful.dfont .. "'")
        end),

    awful.key({ modkey }, "x",
              function ()
                  awful.prompt.run({ prompt = "Run Lua code: " },
                  mypromptbox[mouse.screen].widget,
                  awful.util.eval, nil,
                  awful.util.getdir("cache") .. "/history_eval")
              end)
    -- Menubar
    --awful.key({ modkey }, "p", function() menubar.show() end)
)

clientkeys = awful.util.table.join(
    awful.key({ modkey,           }, "f",      function (c) c.fullscreen = not c.fullscreen  end),
    awful.key({ modkey, "Shift"   }, "c",      function (c) c:kill()                         end),
    awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ),
    awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end),
    awful.key({ modkey,           }, "o",      awful.client.movetoscreen                        ),
    awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end),
    awful.key({ modkey,           }, "n",
        function (c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end),
    awful.key({ modkey,           }, "m",
        function (c)
            c.maximized_horizontal = not c.maximized_horizontal
            c.maximized_vertical   = not c.maximized_vertical
        end)
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    globalkeys = awful.util.table.join(globalkeys,
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = mouse.screen
                        local tag = awful.tag.gettags(screen)[i]
                        if tag then
                           awful.tag.viewonly(tag)
                        end
                  end),
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = mouse.screen
                      local tag = awful.tag.gettags(screen)[i]
                      if tag then
                         awful.tag.viewtoggle(tag)
                      end
                  end),
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      local tag = awful.tag.gettags(client.focus.screen)[i]
                      if client.focus and tag then
                          awful.client.movetotag(tag)
                     end
                  end),
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      local tag = awful.tag.gettags(client.focus.screen)[i]
                      if client.focus and tag then
                          awful.client.toggletag(tag)
                      end
                  end))
end

clientbuttons = awful.util.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 1, awful.mouse.client.move),
    awful.button({ modkey }, 3, awful.mouse.client.resize))

-- Set keys

root.keys(globalkeys)

-- }}}

-- {{{ Rules
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     keys = clientkeys,
                     buttons = clientbuttons } },
    { rule = { class = "MPlayer" },
      properties = { floating = true } },
    { rule = { class = "pinentry" },
      properties = { floating = true } },
    { rule = { class = "gimp" },
      properties = { floating = true } },
    {
        rule = { class = "URxvt", name = "Terminal" },
        callback = function (c)
            awful.client.property.set(c, "overwrite_class", "URxvt:Terminal")
        end
    },
    {
        rule = { class = "URxvt", name = "Drop-Terminal" },
        callback = function (c)
            awful.client.property.set(c, "overwrite_class", "URxvt:Drop")
        end
    },
    {
        rule = { class = "URxvt", name = "Ranger" },
        callback = function (c)
            awful.client.property.set(c, "overwrite_class", "URxvt:Ranger")
        end
    },
    {
        rule = { class = "URxvt", name = "Tmux" },
        callback = function (c)
            awful.client.property.set(c, "overwrite_class", "URxvt:Tmux")
        end
    },

    {
        rule = { class = "URxvt", name = "Chat" },
        callback = function (c)
            awful.client.property.set(c, "overwrite_class", "URxvt:Chat")
        end
    },
    { rule = { class = "URxvt", name = "Music" },
      callback = function (c)
            awful.client.property.set(c, "overwrite_class", "URxvt:MPD")
        end
    },
    {
        rule = { class = "XTerm" },
        callback = function (c)
            awful.client.property.set(c, "overwrite_class", "XTerm:Terminal")
        end
    },
    {
        rule = { class = "mpv" },
        callback = function (c)
            awful.client.property.set(c, "overwrite_class", "MPV:Movie")
        end
    },
    { rule = { class = "Kmail", name = "sin nombre" },
      callback = function (c)
            awful.client.property.set(c, "overwrite_class", "Kmail:New")
        end
    },
    { rule = { class = "Kmail", name = "Re:" },
      callback = function (c)
            awful.client.property.set(c, "overwrite_class", "Kmail:Re")
        end
    },
    {
		rule = { class = "Dolphin", name = "Copiando " },
        callback = function (c)
            awful.client.property.set(c, "overwrite_class", "Dolphin:Copy")
        end
    },

    {
		rule = { class = "Dolphin", name = "Moviendo " },
        callback = function (c)
            awful.client.property.set(c, "overwrite_class", "Dolphin:Move")
        end
    },
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c, startup)
    -- Enable sloppy focus
    c:connect_signal("mouse::enter", function(c)
        if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
            and awful.client.focus.filter(c) then
            client.focus = c
        end
    end)

    if not startup then
        -- Set the windows at the slave,
        -- i.e. put it at the end of others instead of setting it master.
        -- awful.client.setslave(c)

        -- Put windows in a smart way, only if they does not set an initial position.
        if not c.size_hints.user_position and not c.size_hints.program_position then
            awful.placement.no_overlap(c)
            awful.placement.no_offscreen(c)
        end
    end

    local titlebars_enabled = false
    if titlebars_enabled and (c.type == "normal" or c.type == "dialog") then
        -- Widgets that are aligned to the left
        local left_layout = wibox.layout.fixed.horizontal()
        left_layout:add(awful.titlebar.widget.iconwidget(c))

        -- Widgets that are aligned to the right
        local right_layout = wibox.layout.fixed.horizontal()
        right_layout:add(awful.titlebar.widget.floatingbutton(c))
        right_layout:add(awful.titlebar.widget.maximizedbutton(c))
        right_layout:add(awful.titlebar.widget.stickybutton(c))
        right_layout:add(awful.titlebar.widget.ontopbutton(c))
        right_layout:add(awful.titlebar.widget.closebutton(c))

        -- The title goes in the middle
        local title = awful.titlebar.widget.titlewidget(c)
        title:buttons(awful.util.table.join(
                awful.button({ }, 1, function()
                    client.focus = c
                    c:raise()
                    awful.mouse.client.move(c)
                end),
                awful.button({ }, 3, function()
                    client.focus = c
                    c:raise()
                    awful.mouse.client.resize(c)
                end)
                ))

        -- Now bring it all together
        local layout = wibox.layout.align.horizontal()
        layout:set_left(left_layout)
        layout:set_right(right_layout)
        layout:set_middle(title)

        awful.titlebar(c):set_widget(layout)
    end
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}
