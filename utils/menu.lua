beautiful       = require("beautiful")
freedesktop     = require("freedesktop")
capi  = {
            screen = screen,
            tag = tag,
            timer = timer,
            mouse = mouse,
            client = client,
            keygrabber = keygrabber
}

function mvscr(c)
     local scrs = {}
     for s = 1, capi.screen.count() do
     scrs[s] = { s, function () awful.client.movetoscreen(c, s) end }
     end
     return scrs
end

function tag(func, c)
     local tags_n = {}
     for t = 1, #awful.tag.gettags(c.screen) do
     tags_n[t] = { t, function() func(awful.tag.gettags(c.screen)[t], c) end }
     end
     return tags_n
end

function textWrap( str, limit, indent, indent1 )

   limit = limit or 72
   indent = indent or ""
   indent1 = indent1 or indent

   local here = 1 - #indent1
   return indent1..str:gsub( "(%s+)()(%S+)()",
      function( sp, st, word, fi )
         if fi-here > limit then
            here = st - #indent
            return "\n"..indent..word
         end
      end )
end

function clsmenu()
    local cls = capi.client.get()
    local cls_t = {}
    for k, c in pairs(cls) do
		if not awful.rules.match(c, {instance = "Drop-Terminal"}) then
			cls_t[#cls_t + 1] = {
				--textWrap(awful.util.escape(c.name), 30, "", " " ),
				awful.util.escape(c.name),
				function ()
					if not c:isvisible() then
						awful.tag.viewmore(c:tags(), c.screen)
					end
					if c.minimized then
						c.minimized = not c.minimized
					end
					capi.client.focus = c
					c:raise()
					awful.screen.focus(c.screen)
				end,
				c.icon}
		end
    end
    --mytextbox:set_text(cls_t)
    return cls_t
end

function clsmenu_drop()
    local cls = capi.client.get()
    local cls_t = {}
    for k, c in pairs(cls) do
		if awful.rules.match(c, {instance = "Drop-Terminal"}) then
			cls_t[#cls_t + 1] = {
				--textWrap(awful.util.escape(c.name), 30, "", " " ),
				awful.util.escape(c.name),
				function ()
					if not c:isvisible() then
						awful.tag.viewmore(c:tags(), c.screen)
					end
					if c.minimized then
						c.minimized = not c.minimized
					end
					capi.client.focus = c
					awful.client.floating.set(c)
					c:raise()
					awful.screen.focus(c.screen)
				end,
				c.icon}
		end
    end
    --mytextbox:set_text(cls_t)
    return cls_t
end

function setlay(c, arg) -- FIXME set layout for selected client tag
         local lay_m = {}
         for s = 1, #arg do
         lay_m[s] = { awful.layout.getname(arg[s]), function () awful.layout.set(arg[s], awful.tag.selected(c.screen)) end, beautiful["layout_" .. awful.layout.getname(arg[s])] }
         end
         return lay_m
end

myawesomemenu = {
   { "manual", terminal .. " -e man awesome", freedesktop.utils.lookup_icon({ icon = 'help' }) },
   { "edit config", editor_cmd .. " " .. awesome.conffile, freedesktop.utils.lookup_icon({ icon = 'package_settings' }) },
   { "restart", awesome.restart, freedesktop.utils.lookup_icon({ icon = 'gtk-refresh' }) },
   { "quit", awesome.quit, freedesktop.utils.lookup_icon({ icon = 'gtk-quit' }) }
}

exitmenu = {
   { "Shutdown", "kshutdown -s", freedesktop.utils.lookup_icon({ icon='system-shutdown' }) },
   { "Restart", "kshutdown -r", freedesktop.utils.lookup_icon({ icon='view-refresh' }) },
   { "Lock", "slimlock", freedesktop.utils.lookup_icon({ icon='system-lock-screen' }) },
   { "awesome", myawesomemenu, beautiful.awesome_icon }
}

volume = {
   { "Un/Mute", "amixer set Master toggle", freedesktop.utils.lookup_icon({ icon='audio-volume-muted' }) },
   { "30%", "pamixer --set-volume 30", freedesktop.utils.lookup_icon({ icon='audio-volume-low' }) },
   { "50%", "pamixer --set-volume 50", freedesktop.utils.lookup_icon({ icon='audio-volume-low' }) },
   { "80%", "pamixer --set-volume 80", freedesktop.utils.lookup_icon({ icon='audio-volume-medium' }) },
   { "100%", "pamixer --set-volume 100", freedesktop.utils.lookup_icon({ icon='audio-volume-high' }) },
   { "Mixer", volmixer }
}

musicmenu = {
   { "Play/Pause", "mpc toggle", freedesktop.utils.lookup_icon({ icon='stock_media-play' }) },
   { "Stop", "mpc stop", freedesktop.utils.lookup_icon({ icon='stock_media-stop' }) },
   { "Previous", "mpc prev", freedesktop.utils.lookup_icon({ icon='stock_media-prev' }) },
   { "Next", "mpc next", freedesktop.utils.lookup_icon({ icon='stock_media-next' }) },
   { "CLI", music, freedesktop.utils.lookup_icon({ icon='multimedia-volume-control' }) },
   { "GUI", "cantata", freedesktop.utils.lookup_icon({ icon='multimedia-volume-control' }) },
   { "Volume", volume, freedesktop.utils.lookup_icon({ icon='stock_volume' }) }

}

beautiful.default_app_icon = freedesktop.utils.lookup_icon({ icon='application-default-icon' })

mymainmenu = awful.menu({
	theme = {
		border=1,
		border_color = beautiful.border_focus,
		bg_normal = "#000000"
	},
	items = awful.util.table.join({
		{ "&File Manager", fileman, freedesktop.utils.lookup_icon({ icon='file-manager' }) },
		{ "&Browser", browser, freedesktop.utils.lookup_icon({ icon='browser' }) },
		{ "&Terminal", terminal, freedesktop.utils.lookup_icon({ icon='terminal' }) },
		{ " ", function () awful.menu.hide(mymainmenu) end, nil},
		--{ "Data", myplacesmenu.genMenu('/home/data/'), freedesktop.utils.lookup_icon({ icon='folder-home' }) },
		--{ " ", function () awful.menu.hide(mymainmenu) end, nil}
		},
		freedesktop.menu.new(),
		{
		{ " ", function () awful.menu.hide(mymainmenu) end, nil},
		{ "Music", musicmenu, freedesktop.utils.lookup_icon({ icon='audio-headphones' }) },
		{ " ", function () awful.menu.hide(mymainmenu) end, nil},
		{ "Exit", exitmenu, freedesktop.utils.lookup_icon({ icon='exit' }) }}
	)
})

mylauncher = awful.widget.launcher({ bg_normal = "#000000", menu = mymainmenu })
