-- {{{ Required Libraries

gears			= require("gears")
awful			= require("awful")
awful.rules		= require("awful.rules")
awful.autofocus	= require("awful.autofocus")
wibox			= require("wibox")
beautiful		= require("beautiful")
naughty			= require("naughty")
drop            = require("scratchdrop")
lain			= require("lain")
freedesktop		= require("freedesktop")
eminent         = require("eminent")
menubar         = require("menubar")
revelation      = require("revelation")
myplacesmenu    = require("places")
awesompd        = require("awesompd/awesompd")
-- }}}

-- {{{ Error Handling

if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

do
    in_error = false
    awesome.connect_signal("debug::error", function (err)
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = err })
        in_error = false
    end)
end
-- }}}

-- {{{ Variable Definitions

os.setlocale(os.getenv("LANG"))

-- Useful Paths
home = os.getenv("HOME")
confdir = home .. "/.config/awesome"
scriptdir = home .. "/.bin"
scr_res = "1920x1200"

-- Choose Your Theme
beautiful.init(confdir .. "/themes/Darkness/theme.lua")

terminal        = "urxvtc -geometry 100x30 -name Terminal -icon " .. freedesktop.utils.lookup_icon({ icon='terminal' })
drop_terminal   = "urxvtc -pe tabbedex -name Drop-Terminal -icon " .. freedesktop.utils.lookup_icon({ icon='guake' })
editor          = "vim"
editor_cmd      = terminal .. " -e " .. editor
gui_editor      = "gvim"
browser         = "firefox"
fileman         = "dolphin " .. "/home/data/"
cli_fileman     = terminal .. " -title Ranger -icon " .. freedesktop.utils.lookup_icon({ icon='file-manager' }) .. " -e ranger " .. "/home/data"
music           = terminal .. " -title Music -icon " .. freedesktop.utils.lookup_icon({ icon='cantata' }) .. " -e ncmpcpp "
tasks           = terminal .. " -icon " .. freedesktop.utils.lookup_icon({ icon='htop' }) .. " -e htop "
wifi            = terminal .. " -title Network -- -icon " .. freedesktop.utils.lookup_icon({ icon='network-wired'  }) .. " -e nmtui "
chat            = terminal .. " -title Chat -icon " .. freedesktop.utils.lookup_icon({ icon='pidgin'  })  ..  " -e weechat "
tmux            = terminal .. " -title Tmux -icon " .. freedesktop.utils.lookup_icon({ icon='terminator' }) .. " -e tmux new -s arkhan "

-- Default modkey.
modkey = "Mod4"
altkey = "Mod1"

-- }}}

-- {{{ Wallpaper
if beautiful.wallpaper then
    for s = 1, screen.count() do
        gears.wallpaper.maximized(beautiful.wallpaper, s, true)
    end
end
-- }}}

--{{{ Naughty

naughty.config.defaults.timeout = 5
naughty.config.defaults.margin = "5"
naughty.config.defaults.ontop = true
naughty.config.defaults.screen = 1
naughty.config.defaults.position = "top_right"
naughty.config.defaults.border_width = 1
naughty.config.defaults.border_color = beautiful.tooltip_border_color
naughty.config.defaults.bg = "#000000"
naughty.config.defaults.fg = "#FFFFFF"

--}}}

-- {{{ lain & layouts
local layouts =
{
    awful.layout.suit.floating,
    awful.layout.suit.tile,
    awful.layout.suit.tile.left,
    awful.layout.suit.tile.bottom,
    awful.layout.suit.tile.top,
    awful.layout.suit.fair,
    awful.layout.suit.fair.horizontal,
    awful.layout.suit.spiral,
}
-- }}}

-- {{{ Tags

tags = {}
for s = 1, screen.count() do
   tags[s] = awful.tag( { "web", "files", "chat", "media", "extra" }, s, layouts[4])
end

-- }}}

-- {{{ Revelation

revelation.config.tag_name = "rev"
revelation.exact = awful.rules.match
revelation.any   = awful.rules.match_any


-- }}}

-- {{{ Menu
-- Create a laucher widget and a main menu

require("utils.menu")

-- Menubar configuration
menubar.menu_gen.all_menu_dirs = { "/usr/share/applications/", "/usr/local/share/applications", "~/.local/share/applications" }
menubar.utils.terminal = terminal
menubar.g = {
    height = 30,
    width = 300,
    x = 0,
    y = 700
}
-- }}}

-- {{{ Widgets

-- markup
markup             = lain.util.markup
gray               = beautiful.fg_normal
white              = beautiful.fg_focus

-- Clock & Date
mytextclock = awful.widget.textclock(markup(gray, "| ") .. markup(white, "%A %d %B ") .. markup(gray, "/") .. markup(white, " %H:%M "))

-- Calendar with the Clock
lain.widgets.calendar:attach(mytextclock, {
    fg = "#FFFFFF",
    bg = "#000000",
})

-- /home fs
fswidget = lain.widgets.fs({
    partition = '/home',
    settings  = function()
        widget:set_markup(markup(gray, "| Hdd ") .. markup(white, fs_now.used))
    end
})

-- Volume widget
volumewidget = lain.widgets.alsa({
    settings = function()
        if volume_now.status == "off" then
            volume_now.level = volume_now.level .. "M"
        end
        widget:set_markup(markup(gray, "| Vol ") .. markup(white, volume_now.level))
    end
})

-- Music

musicwidget = awesompd:create()
musicwidget.font = beautiful.font
musicwidget.scrolling = true
musicwidget.output_size = 200
musicwidget.update_interval = 10
musicwidget.path_to_icons = confdir .. "/awesompd/icons"
musicwidget.jamendo_format = awesompd.FORMAT_MP3
musicwidget.show_album_cover = true
musicwidget.album_cover_size = 100
musicwidget.mpd_config = home .. "/.config/mpd/mpd.conf"
musicwidget.browser = "firefox"
musicwidget.ldecorator = " "
musicwidget.rdecorator = " "
musicwidget.servers = {
    { server = "localhost", port = 6600 },
}
musicwidget:register_buttons({  { "", awesompd.MOUSE_LEFT, musicwidget:command_toggle() },
                                { "Control", awesompd.MOUSE_SCROLL_UP, musicwidget:command_prev_track() },
                                { "Control", awesompd.MOUSE_SCROLL_DOWN, musicwidget:command_next_track() },
                                { "", awesompd.MOUSE_SCROLL_UP, musicwidget:command_volume_up() },
                                { "", awesompd.MOUSE_SCROLL_DOWN, musicwidget:command_volume_down() },
                                { "", awesompd.MOUSE_RIGHT, musicwidget:command_show_menu() },
                                { "", "XF86AudioLowerVolume", musicwidget:command_volume_down() },
                                { "", "XF86AudioRaiseVolume", musicwidget:command_volume_up() },
                                { modkey, "Pause", musicwidget:command_playpause() } })
musicwidget:run()

-- Net Info
wifiwidget = lain.widgets.base({
  cmd = "iwgetid -r",
  settings = function()
    if output ~= nil and output ~= '' then
      widget:set_markup(output)
    else
      widget:set_markup('')
    end
  end
})

local wifinotification
wifiwidget:connect_signal("mouse::enter", function()
    local f = assert(io.popen("iwgetid -ar"))
    local wifiaccesspoint = f:read()
    f:close()

    local f = io.popen("ip link show | cut -d' ' -f2,9")
    local ws = f:read("*a")
    f:close()

    local ws = ws:match("%w+: UP")

    f = assert(io.popen("iwconfig ".. ws:gsub(": UP", "") .." | grep 'Link Quality' | awk '{print $2}' | awk -F'=' '{print $2}'"))
    local wifiquality = f:read()
    f:close()

    f = io.popen("ip addr | grep 'inet ' | grep -v '127.0.0.1' | cut -d ':' -f2 | awk '{print $2}'")
    local ipaddress = f:read()
    f:close()

wifinotification = naughty.notify({
    text = "IP: " .. ipaddress .. "\nAccess Point: " .. wifiaccesspoint .. "\nLink Quality: " .. wifiquality,
    position = "top_right",
    timeout = 0
  })
end)

wifiwidget:connect_signal("mouse::leave", function()
    if (wifinotification ~= nil) then
        naughty.destroy(wifinotification)
        wifinotification = nil
    end
end)

netwidget = lain.widgets.net({
    settings = function()
        widget:set_markup(markup(gray, " ") .. markup(white, net_now.received) .. markup(gray, " / ") .. markup(white, net_now.sent))
    end
})

netwidget:buttons(awful.util.table.join(
    awful.button({ }, 1, function () awful.util.spawn(wifi) end)
))

-- Battery
batwidget = lain.widgets.bat({
    battery = "BAT0",
    settings = function()
        if bat_now.perc == "N/A" then
            bat_now.perc = "AC "
        else
            bat_now.perc = bat_now.perc
        end
        widget:set_markup(markup(gray, "| Bat ") .. markup(white, bat_now.perc))
    end
})

-- }}}

-- {{{ Spacers

-- Separators

rbracket = wibox.widget.textbox()
rbracket:set_text(']')
lbracket = wibox.widget.textbox()
lbracket:set_text('[')
line = wibox.widget.textbox()
line:set_text('│')
space = wibox.widget.textbox()
space:set_text(' ')

-- }}}

-- {{{ Wibox

mywibox = {}
mybottomwibox = {}
mypromptbox = {}
mylayoutbox = {}
mytaglist = {}
mytaglist.buttons = awful.util.table.join(
                    awful.button({ }, 1, awful.tag.viewonly),
                    awful.button({ modkey }, 1, awful.client.movetotag),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, awful.client.toggletag),
                    awful.button({ }, 4, function(t) awful.tag.viewnext(awful.tag.getscreen(t)) end),
                    awful.button({ }, 5, function(t) awful.tag.viewprev(awful.tag.getscreen(t)) end)
                    )
mytasklist = {}
mytasklist.buttons = awful.util.table.join(
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  c.minimized = false
                                                  if not c:isvisible() then
                                                      awful.tag.viewonly(c:tags()[1])
                                                  end
                                                  client.focus = c
                                                  c:raise()
                                              end
                                          end),
                     awful.button({ }, 3, function ()
                                              if instance then
                                                  instance:hide()
                                                  instance = nil
                                              else
                                                  instance = awful.menu.clients({ width=250 })
                                              end
                                          end),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                              if client.focus then client.focus:raise() end
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                              if client.focus then client.focus:raise() end
                                          end))

for s = 1, screen.count() do

    mypromptbox[s] = awful.widget.prompt()

    mylayoutbox[s] = awful.widget.layoutbox(s)
    mylayoutbox[s]:buttons(awful.util.table.join(
                            awful.button({ }, 1, function () awful.layout.inc(layouts, 1) end),
                            awful.button({ }, 3, function () awful.layout.inc(layouts, -1) end),
                            awful.button({ }, 4, function () awful.layout.inc(layouts, 1) end),
                            awful.button({ }, 5, function () awful.layout.inc(layouts, -1) end)))

    mytaglist[s] = awful.widget.taglist(s, awful.widget.taglist.filter.all, mytaglist.buttons)

    mytasklist[s] = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, mytasklist.buttons)

    mywibox[s] = awful.wibox({ position = "top", screen = s, height = 16 })

    left_layout = wibox.layout.fixed.horizontal()
    left_layout:add(mylayoutbox[s])
    left_layout:add(space)
    left_layout:add(mytaglist[s])
    left_layout:add(space)
    left_layout:add(mypromptbox[s])
    left_layout:add(space)

    right_layout = wibox.layout.fixed.horizontal()
    right_layout:add(space)
    right_layout:add(space)
    right_layout:add(wifiwidget)
    right_layout:add(netwidget)
    right_layout:add(space)
    right_layout:add(fswidget)
    right_layout:add(space)
    right_layout:add(batwidget)
    right_layout:add(space)
    right_layout:add(volumewidget)
    right_layout:add(space)
    right_layout:add(mytextclock)
    right_layout:add(space)
--    right_layout:add(mylayoutbox[s])

    local layout = wibox.layout.align.horizontal()
    layout:set_left(left_layout)
    layout:set_right(right_layout)

    mywibox[s]:set_widget(layout)

    mybottomwibox[s] = awful.wibox({ position = "bottom", screen = s, height = 16 })
    mybottomwibox[s].visible = false

    bottom_right_layout = wibox.layout.fixed.horizontal()
    if s == 1 then bottom_right_layout:add(wibox.widget.systray()) end
    bottom_right_layout:add(space)
    bottom_right_layout:add(musicwidget.widget)

    bottom_layout = wibox.layout.align.horizontal()
    bottom_layout:set_middle(mytasklist[s])
    bottom_layout:set_right(bottom_right_layout)
    mybottomwibox[s]:set_widget(bottom_layout)

end

-- }}}

-- {{{ Mouse bindings
root.buttons(awful.util.table.join(
--    awful.button({ }, 3, function () mymainmenu:toggle() end),
--    awful.button({ }, 4, awful.tag.viewnext),
--    awful.button({ }, 5, awful.tag.viewprev)
))
-- }}}

-- {{{ Key Bindings
 globalkeys = awful.util.table.join(
    awful.key({ modkey,           }, "Left",   awful.tag.viewprev       ),
    awful.key({ modkey,           }, "Right",  awful.tag.viewnext       ),
    awful.key({ modkey,           }, "Escape", awful.tag.history.restore),

    awful.key({modkey}, "e",  revelation ),

    -- ScreenShot
    awful.key({                   }, "Print",      function() os.execute(scriptdir .. "/screenshot") end),

    -- Show/Hide Wibox
    awful.key({ modkey, "Control" }, "F1", function ()
        mywibox[mouse.screen].visible = not mywibox[mouse.screen].visible
    end),

    awful.key({ modkey }, "F1", function ()
        mybottomwibox[mouse.screen].visible = not mybottomwibox[mouse.screen].visible
    end),

    awful.key({ modkey,           }, "j",
        function ()
            awful.client.focus.byidx( 1)
            if client.focus then client.focus:raise() end
        end),
    awful.key({ modkey,           }, "k",
        function ()
            awful.client.focus.byidx(-1)
            if client.focus then client.focus:raise() end
        end),

    -- Show Menu
    awful.key({ modkey }, "w",
        function ()
            mymainmenu:show({ keygrabber = true })
	end),
    -- Show Menu Bar
    awful.key({ modkey }, "s", function () menubar.show() end),


    -- On the fly useless gaps change
    awful.key({ altkey, "Control" }, "+", function () lain.utils.useless_gaps_resize(1) end),
    awful.key({ altkey, "Control" }, "-", function () lain.utils.useless_gaps_resize(-1) end),

    awful.key({ altkey            }, "Tab",
        function ()
            menu = {theme = {width = 300, height = 20, border=1}}
            menu.items = clsmenu()
            local m = awful.menu(menu)
            m:show()
	end),

    awful.key({ modkey            }, "r",
        function()
            os.execute(
                "dmenu_run -b -i -p 'Run:" ..
                "' -nb '" .. beautiful.bg_dmenu_normal ..
                "' -nf '" .. beautiful.fg_normal ..
                "' -sb '" .. beautiful.bg_dmenu_focus  ..
                "' -sf '" .. beautiful.fg_focus  ..
                "' -fn '" .. beautiful.dfont .. "'")
        end),

    -- Widgets popups
    awful.key({ altkey,           }, "c",      function () lain.widgets.calendar:show(7) end),
    awful.key({ altkey,           }, "h",      function () lain.widgets.fs:show(7) end),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end),
    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end),
    awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end),
    awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto),
    awful.key({ modkey,           }, "Tab", function () lain.util.tag_view_nonempty(1) end),
    awful.key({ modkey, "Shift"   }, "Tab", function () lain.util.tag_view_nonempty(-1) end),

    -- Standard program
    awful.key({ modkey,		  }, "p",     function () awful.util.spawn(music)  end),
    awful.key({ modkey,           }, "j",      function() os.execute(scriptdir .. "/jdowloader") end),
    awful.key({ modkey,		  }, "t",     function () awful.util.spawn(fileman) end),
    awful.key({ modkey,		  }, "f",     function () awful.util.spawn(cli_fileman) end),
    awful.key({ modkey,		  }, "b",     function () awful.util.spawn(browser) end),
    awful.key({ modkey,		  }, "i",     function () awful.util.spawn(chat) end),
    awful.key({ modkey,           }, "d",     function () awful.util.spawn(torrent) end),
    awful.key({ modkey,           }, "Return",function () awful.util.spawn(terminal) end),
    awful.key({ modkey, "Control" }, "Return",function () awful.util.spawn(tmux) end),
    awful.key({ modkey, "Control" }, "r", awesome.restart),
    awful.key({ modkey, "Shift"   }, "q", awesome.quit),

    -- Record a Screencast
    awful.key({ modkey,       }, "F7",      function() awful.util.spawn(scriptdir .. "/start_screencast") end),
    awful.key({ modkey,       }, "F8",      function() awful.util.spawn(scriptdir .. "/stop_screencast") end),

     -- Dropdown terminal
    awful.key({               }, "F12",     function () drop(drop_terminal, "top", "center", 0.9, 0.7, false, false) end),

    awful.key({ modkey,           }, "space", function () awful.layout.inc(layouts,  1) end),
    awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(layouts, -1) end),
    awful.key({ modkey, "Control" }, "n", awful.client.restore),

    -- Power Management
    awful.key({ modkey, "Shift"   }, "p",      function() os.execute("kshutdown -s") end),
    awful.key({ modkey, "Shift"   }, "s",      function() os.execute("kshutdown -S") end),
    awful.key({ modkey, "Shift"   }, "r",      function() os.execute("kshutdown -r") end),
    awful.key({ modkey, "Shift"   }, "l",      function() os.execute("slimlock") end),

    -- Music keys
    awful.key({            }, "XF86AudioPlay",     function () os.execute("ncmpcpp toggle" )  end),
    awful.key({            }, "XF86AudioNext",     function () os.execute("ncmpcpp next" )    end),
    awful.key({            }, "XF86AudioPrev",     function () os.execute("ncmpcpp prev" )    end),

    -- Volume control
    awful.key({ altkey }, "Up",
        function ()
            os.execute("pamixer --increase 1")
            volumewidget.update()
        end),
    awful.key({ altkey }, "Down",
        function ()
            os.execute("pamixer --decrease 1")
            volumewidget.update()
        end),
    awful.key({ altkey }, "m",
        function ()
            os.execute("pamixer --toggle-mute")
            volumewidget.update()
        end),

    awful.key({ modkey }, "x",
              function ()
                  awful.prompt.run({ prompt = "Run Lua code: " },
                  mypromptbox[mouse.screen].widget,
                  awful.util.eval, nil,
                  awful.util.getdir("cache") .. "/history_eval")
              end)
)

clientkeys = awful.util.table.join(
    awful.key({ modkey, "Shift"   }, "f",      function (c) c.fullscreen = not c.fullscreen  end),
    awful.key({ modkey, "Shift"   }, "c",      function (c) c:kill()                         end),
    awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ),
    awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end),
    awful.key({ modkey,           }, "o",      awful.client.movetoscreen                        ),
    awful.key({ modkey,           }, "n",
        function (c)
            c.minimized = true
        end),
    awful.key({ modkey,           }, "m",
        function (c)
            c.maximized_horizontal = not c.maximized_horizontal
            c.maximized_vertical   = not c.maximized_vertical
        end)
)

keynumber = 0
for s = 1, screen.count() do
   keynumber = math.min(9, math.max(#tags[s], keynumber));
end

for i = 1, keynumber do
    globalkeys = awful.util.table.join(globalkeys,
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        screen = mouse.screen
                        if tags[screen][i] then
                            awful.tag.viewonly(tags[screen][i])
                        end
                  end),
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      screen = mouse.screen
                      if tags[screen][i] then
                          awful.tag.viewtoggle(tags[screen][i])
                      end
                  end),
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus and tags[client.focus.screen][i] then
                          awful.client.movetotag(tags[client.focus.screen][i])
                      end
                  end),
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus and tags[client.focus.screen][i] then
                          awful.client.toggletag(tags[client.focus.screen][i])
                      end
                  end))
end

clientbuttons = awful.util.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 1, awful.mouse.client.move),
    awful.button({ modkey }, 3, awful.mouse.client.resize))

-- Set keys
musicwidget:append_global_keys()
root.keys(globalkeys)

-- }}}

-- {{{ Rules

awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = true,
                     keys = clientkeys,
                     maximized_vertical = false,
                     maximized_horizontal = false,
                     buttons = clientbuttons,
	                 size_hints_honor = false
                    }
   },
    { rule_any = { class = { "Vlc", "MPlayer", "Umplayer", "Smplayer", "mpv" } },
      properties = { tag = tags[1][4], switchtotag = true },
      callback = function (c)
            awful.placement.centered(c,nil)
        end
    },

    { rule = { class = "Gsharkdown.py" },
      properties = { tag = tags[1][4], switchtotag = true } },

    { rule = { class = "Qalculate" },
      properties = { floating = true } },

    { rule = { class = "SimpleScreenRecorder" },
      properties = { floating = true }
    },

    { rule = { class = "Gimp" },
      properties = { tag = tags[1][4] },
      callback = function (c)
            awful.placement.centered(c,nil)
        end
    },

    { rule = { class = "Xfburn" },
      properties = { tag = tags[1][4] } },

    { rule = { instance = "plugin-container" },
      properties = { floating = true } },

    { rule = { class = "Firefox" },
      properties = { tag = tags[1][1], switchtotag = true },
    },

    { rule = { class = "Iron" },
      properties = { tag = tags[1][1], },
      callback = function (c)
            awful.placement.centered(c,nil)
        end
    },

    { rule = { class = "Firefox" , instance = "DTA" },
      properties = { tag = tags[1][1], floating = true },
      callback = function (c)
            awful.placement.centered(c,nil)
        end
    },

    { rule = { class = "Firefox" , instance = "Toplevel" },
      properties = { tag = tags[1][1], floating = true },
      callback = function (c)
            awful.placement.centered(c,nil)
        end
    },

    { rule = { class = "Firefox" , instance = "Browser" },
      properties = { tag = tags[1][1], floating = true },
      callback = function (c)
            awful.placement.centered(c,nil)
        end
    },

    { rule = { class = "Firefox" , instance = "Descargas" },
      properties = { tag = tags[1][1], floating = true },
      callback = function (c)
            awful.placement.centered(c,nil)
        end
    },

    { rule = { class = "Firefox" , name = "Install user style" },
      properties = { tag = tags[1][1], floating = true },
      callback = function (c)
            awful.placement.centered(c,nil)
        end
    },

    { rule = { class = "Geary" },
      properties = { tag = tags[1][1] },
      callback = function (c)
            awful.placement.centered(c,nil)
        end
    },

    { rule = { class = "Geary", name = "Mensaje nuevo" },
      properties = { tag = tags[1][1], floating = true },
      callback = function (c)
            awful.placement.centered(c,nil)
        end
    },

    { rule = { class = "Geary", name = "Re: " },
      properties = { tag = tags[1][1], floating = true },
      callback = function (c)
            awful.placement.centered(c,nil)
        end
    },

    { rule = { class = "Geary", name = "RE: " },
      properties = { tag = tags[1][1], floating = true },
      callback = function (c)
            awful.placement.centered(c,nil)
        end
    },

    { rule = { class = "Thunderbird" },
      properties = { tag = tags[1][1], switchtotag = true },
      callback = function (c)
            awful.placement.centered(c,nil)
        end
    },

    { rule = { class = "Thunderbird", name = "Redacción: " },
      properties = { tag = tags[1][1], switchtotag = true },
      callback = function (c)
            awful.placement.centered(c,nil)
        end
    },


    { rule = { class = "Kmail" },
      properties ={ tag = tags[1][1] },
      callback = function (c)
            awful.placement.centered(c,nil)
        end
    },

    { rule = { class = "Kmail", name = "sin nombre" },
      properties = { tag = tags[1][1], floating = true },
      callback = function (c)
            awful.placement.centered(c,nil)
        end
    },

    { rule = { class = "URxvt", name = "Network" },
      properties ={  floating = true },
      callback = function (c)
            awful.placement.centered(c,nil)
        end
    },

    { rule = { class = "XTerm" },
      properties = { floating = true },
      callback = function (c)
          awful.placement.centered(c,nil)
        end
    },

    { rule = { class = "URxvt", name = "saidar" },
      properties = { tag = tags[1][5], switchtotag = true, floating = true } },

    { rule = { class = "URxvt", name = "Music" },
      properties = { tag = tags[1][4], switchtotag = true },
      callback = function (c)
            awful.placement.centered(c,nil)
        end
    },

    { rule = { class = "URxvt", name = "Drop-Terminal" },
      properties = { floating = true } },

    { rule = { class = "URxvt", name = "Terminal" },
      properties = { floating = true },
      callback = function (c)
            awful.placement.centered(c,nil)
        end
    },

    { rule = { class = "URxvt", name = "Tmux" },
      properties = { floating = true },
      callback = function (c)
            awful.placement.centered(c,nil)
        end
    },

    { rule = { class = "URxvt", name = "Mail" },
      properties = { tag = tags[1][1], switchtotag = true } },

    { rule = { class = "URxvt", name = "Chat" },
      properties = { tag = tags[1][3], switchtotag = true },
      callback = function (c)
            awful.placement.centered(c,nil)
        end
    },

    { rule = { class = "URxvt", name = "Feed" },
      properties = { tag = tags[1][4], switchtotag = true } },

    { rule = { class = "URxvt", name = "Ranger" },
      properties = { tag = tags[1][2], switchtotag = true },
        callback = function (c)
            awful.placement.centered(c,nil)
        end
    },

    { rule = { class = "URxvt", name = "ranger:~" },
      properties = { tag = tags[1][2], switchtotag = true } },

    { rule = { class = "URxvt", name = "Youtube" },
      properties = { tag = tags[1][1], switchtotag = true } },

    { rule = { name = "gcalctool" },
      properties = { tag = tags[1][5], switchtotag = true, floating = true } },

    { rule = { class = "URxvt", name = "Abook" },
      properties = { tag = tags[1][4], switchtotag = true } },

    { rule = { class = "URxvt", name = "Elinks" },
      properties = { tag = tags[1][1], switchtotag = true } },

    { rule = { class = "Display" },
      properties = { tag = tags[1][1], floating = true } },

    { rule = { class = "Thunar" },
      properties = { tag = tags[1][3], floating = true, switchtotag = true } },

    { rule = { class = "Skype" },
      properties = { tag = tags[1][4], floating = true } },

    { rule = { class = "Pidgin" },
      properties = { tag = tags[1][4] } },

    { rule = { class = "Gwenview" },
      properties = { tag = tags[1][4], switchtotag = true } },


    { rule_any = { class = {"Zathura", "Okular", "Evince"} },
      properties = { tag = tags[1][4], switchtotag = true } },

    { rule = { class = "feh" },
      properties = { tag = tags[1][4], switchtotag = true } },

    { rule = { class = "Dolphin" },
      properties = { tag = tags[1][2], switchtotag = true } },

    { rule = { class = "Dolphin", name = "Copiando " },
      properties = { tag = tags[1][2], switchtotag = true, floating = true },
        callback = function (c)
            awful.placement.centered(c,nil)
        end
    },

    { rule = { class = "Dolphin", name = "Moviendo " },
      properties = { tag = tags[1][2], switchtotag = true, floating = true },
        callback = function (c)
            awful.placement.centered(c,nil)
        end
    },

    { rule = { class = "Ark" },
      properties = { tag = tags[1][2], switchtotag = true, floating = true },
        callback = function (c)
            awful.placement.centered(c,nil)
        end
    },

    { rule = { class = "Gucharmap" },
      properties = { tag = tags[1][3], switchtotag = true } },

    { rule = { class = "Nitrogen" },
      properties = { tag = tags[1][3], switchtotag = true } },

    { rule = { class = "Geany" },
      properties = { tag = tags[1][5], switchtotag = true } },

    { rule = { class = "libreoffice-impress" },
      properties = { tag = tags[1][2], switchtotag = true } },

    { rule = { class = "libreoffice-math" },
      properties = { tag = tags[1][2], floating = true, switchtotag = true } },

    { rule = { class = "libreoffice-calc" },
      properties = { tag = tags[1][2], switchtotag = true } },

    { rule = { class = "Torrent-search" },
      properties = { tag = tags[1][3] } },

    { rule = { class = "TuxGuitar" },
      properties = { tag = tags[1][5] } },

    { rule = { class = "Unetbootin.elf" },
      properties = { tag = tags[1][5], switchtotag = true } },

    { rule = { class = "Bleachbit" },
      properties = { tag = tags[1][3] } },

    { rule = { class = "Gpartedbin" },
      properties = { tag = tags[1][3], switchtotag = true } },
}

-- }}}

-- {{{ Signals

-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c, startup)
    -- Enable sloppy focus
    c:connect_signal("mouse::enter", function(c)
        if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
            and awful.client.focus.filter(c) then
            client.focus = c
        end
    end)

    if not startup then
        -- Set the windows at the slave,
        -- i.e. put it at the end of others instead of setting it master.
        -- awful.client.setslave(c)

        -- Put windows in a smart way, only if they does not set an initial position.
        if not c.size_hints.user_position and not c.size_hints.program_position then
            awful.placement.no_overlap(c)
            awful.placement.no_offscreen(c)
        end
    end

    local titlebars_enabled = false
    if titlebars_enabled and (c.type == "normal" or c.type == "dialog") then
        -- Widgets that are aligned to the left
        local left_layout = wibox.layout.fixed.horizontal()
        left_layout:add(awful.titlebar.widget.iconwidget(c))

        -- Widgets that are aligned to the right
        local right_layout = wibox.layout.fixed.horizontal()
        right_layout:add(awful.titlebar.widget.floatingbutton(c))
        right_layout:add(awful.titlebar.widget.maximizedbutton(c))
        right_layout:add(awful.titlebar.widget.stickybutton(c))
        right_layout:add(awful.titlebar.widget.ontopbutton(c))
        right_layout:add(awful.titlebar.widget.closebutton(c))

        -- The title goes in the middle
        local title = awful.titlebar.widget.titlewidget(c)
        title:buttons(awful.util.table.join(
                awful.button({ }, 1, function()
                    client.focus = c
                    c:raise()
                    awful.mouse.client.move(c)
                end),
                awful.button({ }, 3, function()
                    client.focus = c
                    c:raise()
                    awful.mouse.client.resize(c)
                end)
                ))

        -- Now bring it all together
        local layout = wibox.layout.align.horizontal()
        layout:set_left(left_layout)
        layout:set_right(right_layout)
        layout:set_middle(title)

        awful.titlebar(c):set_widget(layout)
    end
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)

-- }}}
